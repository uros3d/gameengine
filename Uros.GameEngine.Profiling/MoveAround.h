#pragma once
#include <QVector3D>
#include "Behaviour.h"

class MoveAround : public GameEngine::Behaviour
{
private:
	QVector3D _dir;
public:
	explicit MoveAround(GameEngine::GameObject* gameObject);
	void update(double deltaTime) override;
};
