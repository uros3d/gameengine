#include "MoveAround.h"
#include "GameObject.h"
#include "time.h"

MoveAround::MoveAround(GameEngine::GameObject* gameObject)
	: Behaviour(gameObject), _dir(QVector3D(rand() * 1.0 / RAND_MAX - 0.5, 1, rand() * 1.0 / RAND_MAX - 0.5).normalized()) {}

void MoveAround::update(double deltaTime)
{
	gameObject()->transform()->setPosition(gameObject()->transform()->getPosition() + _dir * 100 * deltaTime);
}
