// QT
#include <QCoreApplication>
#include <QApplication>

// GameEngine
#include "Application.h"
#include "ProjectManager.h"
#include "GameObject.h"
#include "Scene/Camera.h"
#include "Scene/Light.h"

#include "CameraController.h"
#include "GameObjectGenerator.h"

using namespace GameEngine;

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	auto project = new Project("Profiling");
	project->setActiveScene(new Scene("MainScene"));
	ProjectManager::instance()->loadProject(project);

	//Load objects into scene

	// Add camera
	auto camera = Camera::create();
	camera->transform()->setPosition(QVector3D(0, 0, 10));
	camera->addComponent<CameraController>();
	camera->getComponent<Camera>()->setSkyBox(SkyBox(QColor()));

	// Add lights

	Light::setAmbientLightColor(QColor(100, 100, 100));

	auto light = Light::create(Light::Point);
	light->transform()->setPosition(QVector3D(-5, 1, -5));
	light->getComponent<Light>()->setColor(Qt::blue);
	light->getComponent<Light>()->setRange(10);

	light = Light::create(Light::Point);
	light->transform()->setPosition(QVector3D(-5, 1, 5));
	light->getComponent<Light>()->setColor(Qt::red);
	light->getComponent<Light>()->setRange(10);

	light = Light::create(Light::Point);
	light->transform()->setPosition(QVector3D(5, 1, -5));
	light->getComponent<Light>()->setColor(Qt::green);
	light->getComponent<Light>()->setRange(10);

	light = Light::create(Light::Point);
	light->transform()->setPosition(QVector3D(5, 1, 5));
	light->getComponent<Light>()->setColor(Qt::yellow);
	light->getComponent<Light>()->setRange(10);

	auto generator = new GameObject();
	generator->addComponent<GameObjectGenerator>();
	generator->markAsStatic();

	if (Application::initialize(a, Settings()))
		Application::run();

	return a.exec();
}
