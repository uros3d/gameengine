#include "GameObjectGenerator.h"
#include "GameObject.h"
#include "MoveAround.h"

GameObjectGenerator::GameObjectGenerator(GameEngine::GameObject* gameObject)
	: Behaviour(gameObject), _elapsed(0) {}

void GameObjectGenerator::update(double deltaTime)
{
	auto go = GameEngine::GameObject::createPrimitive(GameEngine::GameObject::Cube, gameObject()->transform()->getPosition());
	go->addComponent<MoveAround>();
	_elapsed = 0;
}
