#pragma once
#include "Behaviour.h"

class GameObjectGenerator : public GameEngine::Behaviour
{
private:
	double _elapsed;
public:
	explicit GameObjectGenerator(GameEngine::GameObject* gameObject);
	void update(double deltaTime) override;
};
