set VCINSTALLDIR=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC
set QTDIR=C:\Qt\Qt5.5.1\5.5\msvc2013
set RELEASEDIR=%cd%\Win32\Release\
set DEPLOYDIR=%cd%\Win32\Deploy\

del "%DEPLOYDIR%*.*" /F /Q /S
xcopy "%RELEASEDIR%*.*" "%DEPLOYDIR%" /Y /I /S
del "%DEPLOYDIR%*.exp"
del "%DEPLOYDIR%*.lib"

"%QTDIR%\bin\windeployqt.exe" "%DEPLOYDIR%Uros.GameEngine.dll" -release -no-system-d3d-compiler -no-translations -no-angle

del "%DEPLOYDIR%opengl32sw.dll"