[![MATF](http://www.matf.bg.ac.rs/images/matf.gif)](http://www.matf.bg.ac.rs/)
### Matematički fakultet, Beograd

### Uroš Jovanović 1047/2013

Praktični deo master rada na temu ["Arhitektura sistema za implementaciju igara zasnovana na komponentama"](http://www.racunarstvo.matf.bg.ac.rs/MasterRadovi/2016_01_09_Uros_Jovanovic/rad.pdf), jednostavan 3D sistem za implementaciju igara (eng. "3D Engine") - SII.

# Projekti

### Uros.GameEngine

- Bazni projekat
- Sadrži sve bitne tipove koji pokreću i kontrolišu SII
- Definiše komponentni sistem koji se dalje može proširivati

### Uros.GameEngne.Solar

- Demo projekat napravljen koristeći Uros.GameEngine
- Kontrolisanje kamere se vrši tasterima W,A,S,D
- Skokovi ka planetama se vrši numeričkim tasterima ili dvoklikom na planetu
- Pritiskom tastera *Space* kamera prelazi u *overview* režim

### Uros.GameEngine.Tests

- Unit Testovi

# Tehnologije

Softver je napisan u programskom jeziku C++ koristeći:

- [Visual Studio IDE](https://www.visualstudio.com/)
- [QT Framework](https://www.qt.io/)
- [Catch](https://github.com/philsquared/Catch)

# Build (Windows)

- Instalirati [Visual Studio 2013](https://www.visualstudio.com/en-us/news/releasenotes/vs2013-community-vs)
- Instalirati [QT Framework 5.5.1](https://download.qt.io/archive/qt/5.5/5.5.1/) (msvc2013 x86 ili x64)
- Instalirati [QT VS Addin](https://download.qt.io/official_releases/vsaddin/)
- Konfigurisati QT VS Addin, primer:

| Name | Path |
| ------ | ------ |
| 5.5.1 | C:\Qt\Qt5.5.1\5.5\mscvc2013 |
| 5.5.1 x64 | C:\Qt\Qt5.5.1.x64\5.5\mscvc2013_x64 |

# Build (Linux)
- TODO

# Build (OSX)
- TODO

# Deployment

- Konfigurisati deploy_x86(x64).bat skripte tako da QTDIR ukazuje na validnu putanju
- Pokrenuti skriptu iz komandne linije

# Latest Build

- [d3a0204](https://drive.google.com/file/d/0B5tArUSImnLhSnJBb3c2QlliMEk/view?usp=sharing)