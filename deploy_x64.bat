set VCINSTALLDIR=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC
set QTDIR=C:\Qt\Qt5.5.1.x64\5.5\msvc2013_64
set RELEASEDIR=%cd%\x64\Release\
set DEPLOYDIR=%cd%\x64\Deploy\

del "%DEPLOYDIR%*.*" /F /Q /S
xcopy "%RELEASEDIR%*.*" "%DEPLOYDIR%" /Y /I /S
del "%DEPLOYDIR%*.exp"
del "%DEPLOYDIR%*.lib"

"%QTDIR%\bin\windeployqt.exe" "%DEPLOYDIR%Uros.GameEngine.dll" -release -no-system-d3d-compiler -no-translations -no-angle

del "%DEPLOYDIR%opengl32sw.dll"